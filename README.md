# valheim-server

A Valheim Server in a Linux container.

## Prerequisites

- A x86_64 server or VM with at least 2 CPU cores, 4GB of RAM, 50gb storage and some flavor of Linux.

- `docker`, `docker-compose` and `git` installed with permissions set.

## Clone this repo

If you use this repository, the folder structure has already been created for you.

Clone the repository and `cd` into the project's folder.

```sh
git clone https://gitlab.com/mattlewis/valheim-server.git && cd valheim-server
```
## Configuration

The server is configured through a single `.env` file which can and should be edited to reflect your environment. This file is located at `${PWD}/server/valheim.env` and looks like this:

```
SERVER_NAME=Badass Fuckin' Valheim Server
SERVER_PORT=2456
WORLD_NAME=Canibus
SERVER_PASS=password
SERVER_PUBLIC=true
UPDATE_CRON=*/15 * * * *
UPDATE_IF_IDLE=true
TZ=America/Denver
BACKUPS=true
BACKUPS_CRON=0 * * * *
BACKUPS_DIRECTORY=/config/backups
BACKUPS_MAX_AGE=3
PERMISSIONS_UMASK=022
STEAMCMD_ARGS=validate
VALHEIM_PLUS=false
STATUS_HTTP=true
STATUS_HTTP_PORT=80
```

## World files

To change the world file that the server is using, drop your world's files into the `${PWD}/server/config/worlds/` directory and change the `WORLD_NAME=` variable in `${PWD}/server/valheim.env` to match the name of your world exactly.

For example...

`WORLD_NAME=Canibus`

World files will be automatically backed up by the container. This can be changed by editing the `UPDATE_CRON=` variable.

## Usage

Run `docker-compose up -d` from the root directory of this git project. If this is the first time you have run the container, it might take a while to download the game files.

Monitor this progress and other server logs with `docker logs -f valheim`

If you make changes to the `.env` configuration or add a new world file, you will have to restart the container with `docker-compose restart valheim`.